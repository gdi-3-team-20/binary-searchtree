#include <iostream>
#include <queue>
#include <time.h>
#include <random>
#include "SearchTree.hpp"

void test_speicher();
void benchmark_sorted();
void benchmark_random();
void geodaten();

int main(int argc, char **argv)
{
    SearchTree<int> tree;

    // Einfuegen:
    tree.insert(5);
    tree.insert(4);
    tree.insert(3);
    tree.insert(2);
    tree.insert(7);
    tree.insert(8);
    tree.insert(10);
    tree.insert(9);

    // Ausdrucken:
    std::cout << "Tree: " << "\n";
    tree.print();

    // Suchen:
    std::cout << "Suche nach 8, gefunden: " << tree.search(8)->key << "\n";
    std::cout << "Suche nach 33 (nicht im Baum vorhanden), gefunden: " << (void*)tree.search(33) << "\n"; // nullptr

    // Minimum
    std::cout << "Minimum: " << tree.minimum()->key << "\n";

    // Maximum
    std::cout << "Maximum: " << tree.maximum()->key << "\n";


    // Loeschen:
    std::cout << "8 Loeschen\n";
    tree.deleteNode(tree.search(8));
    std::cout << tree << "\n";

    std::cout << "10 Loeschen\n";
    tree.deleteNode(tree.search(10));
    std::cout << tree << "\n";

    // Sonderfall: Wurzel loeschen:
    std::cout << "5 Loeschen (Wurzel)\n";
    tree.deleteNode(tree.search(5));
    std::cout << tree << "\n";


    // Predecessor testen:
    std::cout << "Element vor der 4 loeschen\n";
    std::cout << "Vorgaenger: " << tree.search(4)->predecessor()->key << "\n";
    tree.deleteNode(tree.search(4)->predecessor());
    std::cout << tree << "\n";


    // Successor testen:
    std::cout << "Element nach der 4 loeschen\n";
    std::cout << "Nachfolger: " << tree.search(4)->successor()->key << "\n";
    tree.deleteNode(tree.search(4)->successor());
    std::cout << tree << "\n";

    std::cout << "\nBenchmark (sortiertes Einfuegen) durchfuehren: " << "\n";
    benchmark_sorted();
    std::cout << "\nBenchmark (zufaelliges Einfuegen) durchfuehren: " << "\n";
    benchmark_random();

    std::cout << "\nTest der Geodaten: \n";
    geodaten();

    //test_speicher();

	return 0;
}

/*
 * Funktion zum Testen des Baumes mit Geodaten
 */
void geodaten(){
    char readbuffer[1000];    //Zwischenspeicher
    char datum [25];
    double value;
    double zahlenspeicher[2000];
    int zaehler = 0;

    SearchTree<double> tree;


    FILE *input_dat;
    //https://doi.pangaea.de/10.1594/PANGAEA.814104
    input_dat = fopen("geodaten.tab", "r");

    if (input_dat == NULL)
        printf("Datei oeffnen fehlgeschlagen\n");

    else {
        while (!feof(input_dat)) {  //Solange noch nicht das File-ende erreicht wurde

            char *x = fgets(readbuffer,sizeof(readbuffer)-1,input_dat);

            if (x!=NULL){

                //Wenn das eingelesene in dieses Format passt (Kommentar wird �bersprungen)
                if(sscanf(readbuffer, "%19s %lf",datum, &value)==2) { //19s ist der String in der die Zeitangaben der Daten gespeichert wird
                    tree.insert(value);
                    zahlenspeicher[zaehler] = value;
                    zaehler++;
                }
            }
        }
    }
    fclose(input_dat);

    int min_zahl = 0, max_zahl = zaehler;
    int zufallszahl;

	std::default_random_engine generator;
	std::uniform_int_distribution<int> distribution(min_zahl, max_zahl);

    // Suche nach 20 zuf�lligen Werten aus dem Zahlenspeicher im Baum
	for(int i = 0; i < 20; i++){
        zufallszahl = distribution(generator);
        std::cout << "Suche nach " << zahlenspeicher[zufallszahl] << ", gefunden: " << tree.search(zahlenspeicher[zufallszahl])->key << "\n";
	}

	// Suche nach Werten, die nicht im Baum vorhanden sind
    std::cout << "Suche nach 150.0, gefunden: " << (void*)tree.search(150.0) << "\n";
    std::cout << "Suche nach 133.0, gefunden: " << (void*)tree.search(133.0) << "\n";
    std::cout << "Suche nach -255.25, gefunden: " << (void*)tree.search(-255.25)<< "\n";

}

/*
 * Funktion zum Benchmark des bin�ren Suchbaumes mit Einf�gen in sortierter Reihenfolge von 1,2 ... n
 */
void benchmark_sorted(){

    int anzahl_eintraege[10] = {2000, 4000, 6000, 8000, 10000, 12000, 14000, 16000, 18000, 20000};
    int zufallszahl = 0;

	int min_zahl = 0, max_zahl = (int)1E8;

	std::default_random_engine generator;
	std::uniform_int_distribution<int> distribution(min_zahl, max_zahl);

	clock_t time;

    // Baeume mit 10 verschiedenen groessen Testen
	for (int j = 0; j < 10; j++)
	{
        SearchTree<int> tree;
		time = clock();
        // Werte einf�gen und zufallszahl generieren, da bei zuf�lligem Einf�gen auch Zeit zur Generierung ben�tigt wird
        for(int i = 0; i < anzahl_eintraege[j]; i++){
            zufallszahl = distribution(generator);
            tree.insert(i);
        }

		time = clock() - time;

		std::cout << anzahl_eintraege[j] << " Time Insert: " << time << "ms" << std::endl;

		time = clock();

        // zuf�llige Werte suchen
		for (int i = 0; i < anzahl_eintraege[j]; i++)
		{
			zufallszahl = distribution(generator);
			tree.search(zufallszahl);
		}

		time = clock() - time;

		std::cout << anzahl_eintraege[j] << " Time Search: " << time << "ms" << std::endl;
	}
}

/*
 * Funktion zum Benchmark des bin�ren Suchbaumes mit Einf�gen mit Zufallszahlen
 */
void benchmark_random(){

    int anzahl_eintraege[10] = {200000, 400000, 600000, 800000, 1000000, 1200000, 1400000, 1600000, 1800000, 2000000};
    int zufallszahl = 0;

	int min_zahl = 0, max_zahl = (int)1E8;

	std::default_random_engine generator;
	std::uniform_int_distribution<int> distribution(min_zahl, max_zahl);

	clock_t time;

    // 10 verschiedene Groessen von Baeumen
	for (int j = 0; j < 10; j++)
	{
        SearchTree<int> tree;
		time = clock();
        // Werte einf�gen
        for(int i = 0; i < anzahl_eintraege[j]; i++){
            zufallszahl = distribution(generator);
            tree.insert(zufallszahl);
        }

		time = clock() - time;

		std::cout << anzahl_eintraege[j] << " Time Insert: " << time << "ms" << std::endl;

		time = clock();
        // zuf�llige Werte suchen
		for (int i = 0; i < anzahl_eintraege[j]; i++)
		{
			zufallszahl = distribution(generator);
			tree.search(zufallszahl);
		}

		time = clock() - time;

		std::cout << anzahl_eintraege[j] << " Time Search: " << time << "ms" << std::endl;
	}
}

/*
 * Funktion testet die deleteNode Funktion auf Speicherlacks
 */
void test_speicher()
{
	int anzahl_eintraege = 10000, zufallszahl = 0, index = 0;
	int laenge_laufzeit = (int)5E6;
	int min_zahl = 0, max_zahl = (int)1E8;
	int zahlspeicher[anzahl_eintraege];
	bool doppelt = false;

	SearchTree<int> tree;

	std::default_random_engine generator;
	std::uniform_int_distribution<int> distribution(min_zahl, max_zahl);

	//Baum mit anzahl_eintraege unterschiedlichen Zahlen fuellen
	for (int i = 0; i < anzahl_eintraege;)
	{
		zufallszahl = distribution(generator);
		doppelt = false;
		for (int j = 0; j < anzahl_eintraege; j++)
		{
		    //Pruefen ob die generiete Zufallszahl bereits vorhanden ist, sonst einfuegen
			if (zahlspeicher[j] == zufallszahl)
			{
				doppelt = true;
				break;
			}
		}

		if (doppelt == false)
		{
			zahlspeicher[i] = zufallszahl;
			tree.insert(zufallszahl);
			i++;
		}
	}

    //Schleife l�uft durch den ganzen Baum und ersetzt die vorhandenen Werte
	for (int k = 0; k < laenge_laufzeit; k++)
	{
		zufallszahl = distribution(generator);
		doppelt = false;
		for (int j = 0; j < anzahl_eintraege; j++)
		{
			if (zahlspeicher[j] == zufallszahl)
			{
				doppelt = true;
				break;
			}
		}

		//Alter Wert wird geloescht, neuer eingefuegt
		if (doppelt == false)
		{
			tree.deleteNode(tree.search(zahlspeicher[index]));
			zahlspeicher[index] = zufallszahl;
			tree.insert(zufallszahl);
			index++;
		}
		if (index >= anzahl_eintraege)
		{
			index = 0;
		}
	}
	return;
}
