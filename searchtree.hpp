#include <iostream>

template <typename T>
class SearchTree;

template <typename T>
class TreeNode
{
	friend class SearchTree<T>;

public:
	TreeNode *parent = nullptr;
	TreeNode *left = nullptr;
	TreeNode *right = nullptr;

public:
	const T key;
	// Konstruktor
	TreeNode(const T rootKey) : key(rootKey) {}
	// Dekonstruktor
	virtual ~TreeNode()
	{
		delete this->left;
		delete this->right;
	}

	// Disallow (accidental) copying or moving:
	TreeNode(const TreeNode &copyFrom) = delete;
	TreeNode(TreeNode &&moveFrom) = delete;
	TreeNode *predecessor();
	TreeNode *successor();
	TreeNode *minimum();
	TreeNode *maximum();



	// optional, aber praktisch zum debuggen:
	friend std::ostream &operator<<(std::ostream &cout, const TreeNode *tree)
	{
		if (tree == nullptr)
			return cout; // nothing to print

		cout << tree->left << tree->key << ", " << tree->right;

		return cout;
	}

private:

	static TreeNode *search(TreeNode *root, const T key);
};

/*
 * Funktion predecessor ermittelt den Vorgänger eines Knotens
 */
template <class T>
TreeNode<T> *TreeNode<T>::predecessor()
{
	TreeNode *succ = this;
	TreeNode *desc = this;
	// Maximum im linken Teilbaum des aktuellen Knotens
	if (succ->left != nullptr)
		return succ->left->maximum();
	desc = succ->parent;
	// Vorgaenger ermitteln wenn kein linker Teilbaum vorhanden ist
	while (desc != nullptr && succ == desc->left)
	{
		succ = desc;
		desc = desc->parent;
	}
	return desc;
}

/*
 * Funktion successor ermittelt den Nachfolger eines Knotens
 */
template <class T>
TreeNode<T> *TreeNode<T>::successor()
{
	TreeNode *succ = this;
	TreeNode *desc = this;
	// Minimum im rechten Teilbaum des aktuellen Knotens
	if (succ->right != nullptr)
		return succ->right->minimum();
	desc = succ->parent;
	// Nachfolger ermitteln wenn kein rechter Teilbaum vorhanden ist
	while (desc != nullptr && succ == desc->right)
	{
		succ = desc;
		desc = desc->parent;
	}
	return desc;
}

/*
 * Methode um den kleinsten Knoten im Baum zu finden
 */
template <class T>
TreeNode<T> *TreeNode<T>::minimum()
{
	TreeNode *min = this;
	// Solange der aktuelle Knoten ungleich nullptr ist wird im Baum nach links gegangen
	// Da dort der kleinste Knoten ist
	while (min->left != nullptr)
		min = min->left;
	return min;
}

/*
 * Methode um den größten Knoten im Baum zu finden
 */
template <class T>
TreeNode<T> *TreeNode<T>::maximum()
{
	TreeNode *max = this;
	// Solange der aktuelle Knoten ungleich nullptr ist wird im Baum nach rechts gegangen
	// Da dort der größte Knoten ist
	while (max->right != nullptr)
		max = max->right;
	return max;
}

template <typename T>
class SearchTree
{
	using Node = TreeNode<T>; // optional, Fuer uebersichtlichen Code

private:
	Node *root; // Wurzel (im Falle eines leeren Baumes: nullptr)

public:
	SearchTree() : root(nullptr) {}
	virtual ~SearchTree() { delete root; }

	// Disallow (accidental) copying or moving:
	SearchTree(const SearchTree &copyFrom) = delete;
	SearchTree(SearchTree &&moveFrom) = delete;

private:
	void transplant(const Node *const nodeToReplace, Node *const replacementNode); // internally used by void delete_node(...)
	void printSubtree(Node *tree, int depth);

public:
	void insert(const T key);
	void deleteNode(Node *const node); // "const Node *const node" nicht zulaessig, da node sonst nicht korrekt geloescht werden koennte
	Node *search(const T key);

	Node *minimum() { return root->minimum(); }
	Node *maximum() { return root->maximum(); }

	void print();

public:
	// optional, aber praktisch zum debuggen:
	friend std::ostream &operator<<(std::ostream &cout, const SearchTree &tree)
	{
		// cout << tree.root; // markiert rootNode nicht
		cout << tree.root->left << "<" << tree.root->key << ">, " << tree.root->right; // markiert rootNode
		return cout;
	}
};

/*
 * Insert Methode
 * Fügt einen Knoten an die richtige Stelle in den Baum ein
 */
template <class T>
void SearchTree<T>::insert(const T key)
{
	Node *z = new Node(key);
	Node *y = nullptr;
	Node *x = root;

	// Startet an der Wurzel des Baumes
	// Zeiger x sucht nach nlptr, das durch z ersetzt werden kann
	// Bewirkt, dass Zeiger im Baum abwärts laufen und dabei nach links oder rechts gehenen
	while (x != nullptr)
	{
		y = x;
		if (z->key < x->key)
			x = x->left;
		else
			x = x->right;
	}
	// Nach der Schleife sind wird einen Schritt zu weit
	// Setzt den Zeiger, sodass es an der richtigen Stelle z eingefügt wird
	z->parent = y;
	if (y == nullptr)
		root = z;
	else if (z->key < y->key)
		y->left = z;
	else
		y->right = z;
}


template <class T>
TreeNode<T> *SearchTree<T>::search(const T key)
{
	return TreeNode<T>::search(root, key);
}

/*
 * Search Methode
 * Sucht die gewünschte Node.
 */
template <class T>
TreeNode<T> *TreeNode<T>::search(TreeNode *root, const T key)
{
	// Überprüft, ob der Wert zur Wurzel passt und gibt die Wurzel zurück.
	if (root == nullptr || key == root->key)
		return root;
	// Ist der Wert kleine oder größer, dann wird dementsprechend zurück gegeben.	
	if (key < root->key)
		return TreeNode<T>::search(root->left, key);
	else
		return TreeNode<T>::search(root->right, key);
}

/*
 * Methode zum "Umhängen" eines Teilbaums, die einen Teilbaum als ein Kind seines Vaters
 * durch einen anderen Teilbaum ersetzt
 * Wird zum löschen eines Knotens benötigt
 */
template <typename T>
void SearchTree<T>::transplant(const Node *const nodeToReplace, Node *const replacement)
{
	// Wenn der zuersetzende Knoten die Wurzel von T ist, wird der Knoten ersetzt
	if (nodeToReplace->parent == nullptr)
		root = replacement;
	// Andernfalls ist der zuersetzende Knoten enweder linkes oder rechtes Kind seines Vaters
	else if (nodeToReplace == nodeToReplace->parent->left)
		nodeToReplace->parent->left = replacement;
	else
		nodeToReplace->parent->right = replacement;
	// Wenn der ersetzte Knoten nicht Null ist, wird der Vater überschrieben
	if (replacement != nullptr)
		replacement->parent = nodeToReplace->parent;
}


/*
 * Methode deleteNode arbeitet mit Hilfe der Methode transplant
 * und bekommt die Node übergeben, die gelöscht werden soll.
 */
template <typename T>
void SearchTree<T>::deleteNode(Node *const node)
{
	// Knoten hat nur ein rechtes Kind
	if (node->left == nullptr)
	{
		transplant(node, node->right);
	}
	// Knoten hat nur ein linkes Kind
	else if (node->right == nullptr)
	{
		transplant(node, node->left);
	}
	// Knoten hat ein linkes und ein rechtes Kinde
	else
	{
		// Nachfolger vom Knoten
		Node *y = node->right->minimum();
		// Nachfolger ist nicht das rechte Kind
		if (y->parent != node)
		{
			// Nachfolger und rechtes Kind vertauschen
			transplant(y, y->right);

			y->right = node->right;
			y->right->parent = y;
		}

		// Knoten mit Nachfolger vertauschen
		transplant(node, y);

		// linkes Kind des Knotens als linkes Kind des Nachfolgers
		y->left = node->left;
		y->left->parent = y;
	}

	// Knoten loeschen
	node->left = nullptr;
	node->right = nullptr;
	delete node;
}


/*
 * Methode print, sorgt für die Ausgabe.
 */
template <class T>
void SearchTree<T>::print()
{
	std::cout << "-----------------------------------------\n";
	printSubtree(root, 0);
	std::cout << "-----------------------------------------\n";
}

/*
 * Methode zur Ausgabe von dem Baum
 * Arbeitet Rekursiv
 */
template <class T>
void SearchTree<T>::printSubtree(Node *tree, int depth)
{
	// Wenn der Baum leer ist, wird die Funktion verlassen
	if (tree == nullptr)
		return;
	// Funktion wird erneut aufgerufen, damit jeder Knoten vom rechten Teil des Baumes gedruckt wird
	printSubtree(tree->right, depth + 1);

	// Schleife, die leerzeichen ausgibt
	// Für die Formatierung.
	for (int i = 0; i < depth; i++)
	{
		std::cout << "     ";
	}
	// Zahl und Farb Eigenschaft wird ausgegeben
	std::cout << "[" << tree->key
			  << "]"
			  << "\n";
	// Funktion wird erneut aufgerufen, damit jeder Knoten vom linken Teil des Baumes gedruckt wird
	printSubtree(tree->left, depth + 1);
}
